<?php

namespace App\Domain\Company\Repositories;

use App\Domain\Company\Contracts\CompanyRepositoryInterface;
use App\Http\Requests\CompanyRequest;
use App\Http\Requests\CompanyUpdateRequest;
use App\Models\Company;
use Illuminate\Database\Eloquent\Collection;

class CompanyRepository implements CompanyRepositoryInterface
{

    public function index()
    {
        return Company::orderBy('id', 'desc')->paginate(10);
    }

    public function create(CompanyRequest $request): Company
    {
        return Company::create($request->validated());
    }

    public function update(Company $company, CompanyUpdateRequest $request): Company
    {
        $company->update($request->validated());

        return $company;
    }

    public function delete(Company $company): void
    {
        $company->delete();
    }
}
