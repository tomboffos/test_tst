<?php

namespace App\Domain\Company\Contracts;

interface CompanyContract
{
    const EMAIL = 'email';
    const NAME = 'name';
    const LOGO = 'logo';
    const SITE = 'site';


    const FILLABLE = [
        self::EMAIL,
        self::NAME,
        self::LOGO,
        self::SITE
    ];
}
