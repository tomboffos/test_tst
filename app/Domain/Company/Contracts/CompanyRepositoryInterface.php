<?php

namespace App\Domain\Company\Contracts;

use App\Http\Requests\CompanyRequest;
use App\Http\Requests\CompanyUpdateRequest;
use App\Models\Company;

interface CompanyRepositoryInterface
{
    public function index();

    public function create(CompanyRequest $request): Company;

    public function update(Company $company, CompanyUpdateRequest $request): Company;

    public function delete(Company $company): void;
}
