<?php

namespace App\Domain\Employee\Repositories;

use App\Domain\Employee\Contracts\EmployeeRepositoryInterface;
use App\Http\Requests\EmployeeStoreRequest;
use App\Http\Requests\EmployeeUpdateRequest;
use App\Models\Employee;

class EmployeeRepository implements EmployeeRepositoryInterface
{

    public function index()
    {
        return Employee::orderBy('id', 'desc')->paginate(10);
    }

    public function create(EmployeeStoreRequest $request): Employee
    {
        return Employee::create($request->validated());
    }

    public function update(Employee $employee, EmployeeUpdateRequest $request): Employee
    {
        $employee->update($request->validated());

        return $employee;
    }

    public function delete(Employee $employee): void
    {
        $employee->delete();
    }
}
