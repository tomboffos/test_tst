<?php

namespace App\Domain\Employee\Contracts;

interface EmployeeContract
{
    const NAME = 'name';
    const SURNAME = 'surname';
    const COMPANY_ID = 'company_id';
    const EMAIL = 'email';
    const PHONE = 'phone';


    const FILLABLE = [
        self::EMAIL,
        self::NAME,
        self::SURNAME,
        self::COMPANY_ID,
        self::PHONE
    ];
}
