<?php

namespace App\Domain\Employee\Contracts;

use App\Http\Requests\EmployeeStoreRequest;
use App\Http\Requests\EmployeeUpdateRequest;
use App\Models\Employee;

interface EmployeeRepositoryInterface
{
    public function index();

    public function create(EmployeeStoreRequest $request): Employee;

    public function update(Employee $employee, EmployeeUpdateRequest $request): Employee;

    public function delete(Employee $employee) : void;
}
