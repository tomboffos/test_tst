<?php

namespace App\Domain\User\Contracts;

interface UserContract
{
    const NAME = 'name';
    const EMAIL = 'email';
    const EMAIL_VERIFIED_AT = 'email_verified_at';
    const PASSWORD = 'password';
    const IS_ADMIN = 'is_admin';


    const FILLABLE = [
        self::NAME,
        self::EMAIL,
        self::EMAIL_VERIFIED_AT,
        self::PASSWORD,
        self::IS_ADMIN,
    ];
}
