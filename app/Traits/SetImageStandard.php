<?php

namespace App\Traits;

use App\Domain\Company\Contracts\CompanyContract;

trait SetImageStandard
{

    public function saveImage($value): array|string|null
    {
        return is_string($value) || is_null($value) ? $this->getOriginal(CompanyContract::LOGO) : str_replace('public/', '', asset('storage' . $value->store('public/')));
    }

    public function setLogoAttribute($value) : void
    {
        $this->attributes[CompanyContract::LOGO] = $this->saveImage($value);
    }

}
