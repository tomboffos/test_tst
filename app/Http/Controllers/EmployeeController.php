<?php

namespace App\Http\Controllers;

use App\Domain\Company\Contracts\CompanyRepositoryInterface;
use App\Domain\Employee\Contracts\EmployeeRepositoryInterface;
use App\Http\Requests\EmployeeStoreRequest;
use App\Http\Requests\EmployeeUpdateRequest;
use App\Models\Employee;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class EmployeeController extends Controller
{

    public function __construct(
        public EmployeeRepositoryInterface $employeeRepository,
        public CompanyRepositoryInterface  $companyRepository
    )
    {

    }

    public function index(): View
    {
        return view('employees.index', [
            'employees' => $this->employeeRepository->index(),
        ]);
    }


    public function create(): View
    {
        return view('employees.create', [
            'companies' => $this->companyRepository->index(),
        ]);
    }


    public function store(EmployeeStoreRequest $request): RedirectResponse
    {
        $this->employeeRepository->create($request);

        return redirect()->route('employees.index');
    }


    public function show(Employee $employee): View
    {
        return view('employees.show', [
            'employee' => $employee
        ]);
    }


    public function edit(Employee $employee): View
    {
        return view('employees.update', [
            'employee' => $employee,
            'companies' => $this->companyRepository->index()
        ]);
    }


    public function update(EmployeeUpdateRequest $request, Employee $employee): RedirectResponse
    {
        $this->employeeRepository->update($employee, $request);

        return redirect()->route('employees.index');
    }

    public function destroy(Employee $employee): RedirectResponse
    {
        $this->employeeRepository->delete($employee);

        return redirect()->route('employees.index');
    }
}
