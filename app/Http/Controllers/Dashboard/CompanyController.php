<?php

namespace App\Http\Controllers\Dashboard;


use App\Domain\Company\Contracts\CompanyRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;
use App\Http\Requests\CompanyUpdateRequest;
use App\Models\Company;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class CompanyController extends Controller
{

    public function __construct(
        protected CompanyRepositoryInterface $companyRepository
    )
    {

    }

    public function index(): View
    {
        return view('companies.index', [
            'companies' => $this->companyRepository->index()
        ]);
    }


    public function create(): View
    {
        return view('companies.create');
    }


    public function store(CompanyRequest $request): RedirectResponse
    {
        $this->companyRepository->create($request);

        return redirect()->route('companies.index');
    }


    public function show(Company $company): View
    {
        return view('companies.show', ['company' => $company]);
    }


    public function edit(Company $company): View
    {
        return view('companies.update', [
            'company' => $company
        ]);
    }


    public function update(CompanyUpdateRequest $request, Company $company) : RedirectResponse
    {
        $this->companyRepository->update($company, $request);

        return redirect()->route('companies.index');
    }


    public function destroy(Company $company) : RedirectResponse
    {
        $this->companyRepository->delete($company);


        return redirect()->route('companies.index');
    }
}
