<?php

namespace App\Http\Requests;

use App\Domain\Employee\Contracts\EmployeeContract;
use Illuminate\Foundation\Http\FormRequest;

class EmployeeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return 1;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            //
            EmployeeContract::NAME => 'required',
            EmployeeContract::SURNAME => 'required',
            EmployeeContract::PHONE => 'nullable',
            EmployeeContract::EMAIL => 'nullable',
            EmployeeContract::COMPANY_ID => 'required|exists:companies,id'
        ];
    }
}
