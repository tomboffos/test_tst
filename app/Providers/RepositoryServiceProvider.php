<?php

namespace App\Providers;

use App\Domain\Company\Contracts\CompanyRepositoryInterface;
use App\Domain\Company\Repositories\CompanyRepository;
use App\Domain\Employee\Contracts\EmployeeRepositoryInterface;
use App\Domain\Employee\Repositories\EmployeeRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CompanyRepositoryInterface::class, CompanyRepository::class);
        $this->app->bind(EmployeeRepositoryInterface::class, EmployeeRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
