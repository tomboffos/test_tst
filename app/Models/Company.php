<?php

namespace App\Models;

use App\Domain\Company\Contracts\CompanyContract;
use App\Traits\SetImageStandard;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property string $email,
 * @property string $name,
 * @property string $site;
 * @property string $logo;
 */
class Company extends Model
{
    use HasFactory, SoftDeletes, SetImageStandard;

    protected $fillable = CompanyContract::FILLABLE;
}
