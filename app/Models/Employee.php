<?php

namespace App\Models;

use App\Domain\Employee\Contracts\EmployeeContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property string $name
 * @property string $surname
 * @property Company $company
 * @property string $email
 * @property string $phone
 **/

class Employee extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = EmployeeContract::FILLABLE;

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
