<?php

use App\Domain\Employee\Contracts\EmployeeContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->foreignId(EmployeeContract::COMPANY_ID)->constrained();
            $table->string(EmployeeContract::NAME);
            $table->string(EmployeeContract::SURNAME);
            $table->string(EmployeeContract::EMAIL)->nullable();
            $table->string(EmployeeContract::PHONE)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
};
