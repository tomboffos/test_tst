<?php

namespace Database\Seeders;

use App\Domain\User\Contracts\UserContract;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!User::whereIsAdmin(true)->exists())
            User::create([
                UserContract::NAME => 'admin',
                UserContract::PASSWORD => 'password',
                UserContract::EMAIL => 'admin@admin.com',
                UserContract::IS_ADMIN => 1
            ]);
    }
}
