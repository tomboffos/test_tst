@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="d-flex align-items-center">
                            <a href="{{route('home')}}" class="btn btn-primary "
                               style="margin-right: 20px;">{{__('Back')}}</a>

                            <h5>
                                {{__('Employees')}}
                            </h5>
                        </div>
                        <a href="{{route('employees.create')}}" class="btn btn-primary">
                            {{__('Create employees')}}
                        </a>
                    </div>
                    <div class="card-body">
                        @foreach($employees as $employee)
                            <div class="d-flex align-items-center mb-2">
                                <div class="w-25">
                                    {{$employee->name}} {{$employee->surname}}
                                </div>
                                <div class="w-25">
                                    {{$employee->email}}
                                </div>
                                <div class="w-25">
                                    <a href="tel:{{$employee->phone}}">
                                        {{$employee->phone}}
                                    </a>
                                </div>
                                <div class="w-25">
                                    {{$employee->company->name}}
                                </div>
                                <div class="w-25">
                                    <a href="{{route('employees.show', ['employee' => $employee])}}"
                                       class="btn btn-primary">
                                        {{__('Show')}}
                                    </a>

                                    <a href="{{route('employees.edit',['employee' => $employee])}}"
                                       class="btn btn-primary">
                                        {{__('Edit')}}
                                    </a>
                                    <form action="{{route('employees.destroy',['employee' => $employee])}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">

                                        <button type="submit" class="btn-primary btn mt-2">
                                            {{__('Delete')}}
                                        </button>
                                    </form>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
