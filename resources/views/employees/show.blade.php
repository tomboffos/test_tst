@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="d-flex align-items-center">
                            <a href="{{route('employees.index')}}" class="btn btn-primary " style="margin-right: 20px;">{{__('Back')}}</a>

                            <h5>
                                {{__('Employees')}}
                            </h5>
                        </div>

                    </div>
                    <div class="card-body">
                        Name: {{$employee->name}} {{$employee->surname}}
                        <br>
                        Email: {{$employee->email}}
                        <br>
                        Phone: {{$employee->phone}}
                        <br>
                        Company: {{$employee->company->name}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
