@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="d-flex align-items-center">
                            <a href="{{route('employees.index')}}" class="btn btn-primary " style="margin-right: 20px;">{{__('Back')}}</a>

                            <h5>
                                {{__('Employees')}}
                            </h5>
                        </div>

                    </div>
                    <div class="card-body">

                        <form action="{{route('employees.store')}}" method="POST">
                            @csrf
                            <div class="form-group mb-2">
                                <label for="name">{{__('Name')}}</label>
                                <input type="text" class="form-control" name="name" id="name" required>
                            </div>
                            <div class="form-group mb-2">
                                <label for="surname">{{__('Surname')}}</label>
                                <input type="text" class="form-control" name="surname" id="surname" required>
                            </div>
                            <div class="form-group mb-2">
                                <label for="email">{{__('Email')}}</label>
                                <input type="text" class="form-control" name="email" id="email">
                            </div>
                            <div class="form-group mb-2">
                                <label for="phone">{{__('Phone')}}</label>
                                <input type="tel" class="form-control" name="phone" id="phone">
                            </div>

                            <div class="form-group mb-2">
                                <label for="company">{{__('Company')}}</label>
                                <select name="company_id" id="company" class="form-control" required>

                                    @foreach($companies as $company)
                                        <option value="{{$company->id}}">{{$company->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group-mb-2">
                                <button type="submit" class="btn btn-primary">
                                    {{__('Submit')}}
                                </button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
