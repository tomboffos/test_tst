@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body w-100">
                    <a href="{{route('companies.index')}}" class="btn btn-primary">
                        {{__('Companies')}}
                    </a>
                    <br>
                    <a href="{{route('employees.index')}}" class="btn btn-primary mt-2 mb-2">
                        {{__('Employees')}}
                    </a>
                    <br>


                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
