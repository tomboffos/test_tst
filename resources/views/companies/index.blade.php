@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <div class="d-flex align-items-center">
                            <a href="{{route('home')}}" class="btn btn-primary "
                               style="margin-right: 20px;">{{__('Back')}}</a>

                            <h5>
                                {{__('Companies')}}
                            </h5>
                        </div>
                        <a href="{{route('companies.create')}}" class="btn btn-primary">
                            {{__('Create company')}}
                        </a>
                    </div>
                    <div class="card-body">
                        @foreach($companies as $company)
                            <div class="d-flex align-items-center">
                                <div class="w-25">
                                    <img src="{{$company->logo}}" width="50px" height="50px" class="" alt="">
                                </div>
                                <div class="w-50 ">
                                    {{$company->name}}
                                </div>

                                <div class="w-25 justify-content-between">
                                    <a href="{{route('companies.show',['company' => $company->id])}}"
                                       class="btn btn-primary">
                                        {{__('Show')}}
                                    </a>

                                    <a href="{{route('companies.edit',['company' => $company->id])}}"
                                       class="btn btn-primary">
                                        {{__('Edit')}}
                                    </a>

                                    <form action="{{route('companies.destroy',['company' => $company->id])}}"
                                          method="POST">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button type="submit"
                                           class="btn btn-primary mb-2 mt-2">
                                            {{__('Delete')}}
                                        </button>
                                    </form>
                                </div>


                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
