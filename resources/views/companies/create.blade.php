@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{route('companies.index')}}" class="btn-primary btn">
                            {{__('Back')}}
                        </a>
                    </div>

                    <div class="card-body">
                        <form action="{{route('companies.store')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group mb-2">
                                <label for="name">{{__('Name')}}</label>
                                <input type="text" class="form-control" name="name" id="name" required>
                            </div>
                            <div class="form-group mb-2">
                                <label for="email">{{__('Email')}}</label>
                                <input type="text" class="form-control" name="email" id="email">
                            </div>
                            <div class="form-group mb-2">
                                <label for="site">{{__('Site')}}</label>
                                <input type="text" class="form-control" name="site" id="site">
                            </div>

                            <div class="form-group mb-2">
                                <label for="logo">{{__('Logo')}}</label>
                                <input type="file" class="form-control" name="logo" id="logo"
                                       accept="image/png, image/jpeg image/png" required>
                            </div>
                            <div class="form-group-mb-2">
                                <button type="submit" class="btn btn-primary">
                                    {{__('Submit')}}
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
