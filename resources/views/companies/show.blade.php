@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{route('companies.index')}}" class="btn btn-primary">Back</a>
                    </div>
                    <div class="card-body">
                        <div class="">
                            <img src="{{$company->logo}}" width="100px" alt="">
                        </div>
                        <div class="mt-5">
                            <h4>{{$company->name}}</h4>
                        </div>
                        <div class="mt-1">
                            Email : <a href="mailto:{{$company->email}}">{{$company->email}}</a>
                        </div>
                        <div class="mt-1">
                            Site: <a href="{{$company->site}}">{{$company->site}}</a>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
